## Cycle to Work Schemes

This project aims to give a clear overview and unbiased comparison between different Cycle to Work schemes available in the UK.

All the information is open and can be improved by anyone; if you notice any inconsistencies, problems or missing details, please [open an issue](https://gitlab.com/olliebennett/cycle-to-work-schemes/issues).
