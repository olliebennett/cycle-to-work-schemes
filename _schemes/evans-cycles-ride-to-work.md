---
layout: scheme
title: Evans Cycles Ride-to-Work
website: https://www.evanscycles.com/b2b/ride-to-work-employee
trustpilot:
logo: https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/dc382c38fcef10d3a67866c25e095d3e/evans-cycles.png
---

About Evans Cycles Scheme

- Includes a (5-10%) voucher for use on accessories.

After the hire period, you can complete an "Extended Use Agreement" to use the bike for a further 4-5 years free of any extra charges.
