---
layout: scheme
title: Cycle2Work
website: https://www.cycle2work.info/
trustpilot:
summary: 'Scheme run by Halfords / CycleRepublic'
logo: https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/fe1ba41bc0a2470f95abd18e77587a22/cycle2work.png
---

Completely free to the employer ([src](https://www.cycle2work.info/employers/why-choose-cycle2work))

Offers various other benefits to employees such as discount cards.

Bikes available from all shops of Halfords and Cycle Republic (owned by Halfords).
