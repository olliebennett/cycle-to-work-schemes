---
layout: default
title: Others
website:
trustpilot:
summary: A selection of other, smaller providers exist but aren't covered in detail here.
---

Here's a list of other, smaller scheme providers

<img src="https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/25f906a3b5d234bd195e6d482d42e25d/c2w-support.png" height="80px" alt="C2W Support"/>

- A minor player with little information available.
- Supported by [Evans Cycles]({{ site.baseurl }}{% link _shops/evans-cycles.md %}) [src](https://www.evanscycles.com/b2b/cycle-to-work-scheme).
- [Visit website](https://www.c2w-support.co.uk/)

<img src="https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/f4e6534a374da371db9941b899fd8a0a/enjoy-benefits.jpg" height="80px" alt="Enjoy Benefits"/>

- A varied package of benefits including Cycle to Work
- [Visit website](https://www.enjoybenefits.co.uk/staff-benefits-savings/cycle-to-work)

<img src="https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/58e777a21b37e041246a4618a60e442e/vivup.png" height="80px" alt="Vivup"/>

- Only for members of the broader employee "benefits, health and wellbeing" platform.
- Supported in [Evans Cycles]({{ site.baseurl }}{% link _shops/evans-cycles.md %}) stores [src](https://www.evanscycles.com/b2b/cycle-to-work-scheme).
- [Visit website](https://www.vivup.co.uk/employer/cycle_to_work)
