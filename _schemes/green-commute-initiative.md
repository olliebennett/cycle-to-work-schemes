---
layout: scheme
title: Green Commute Initiative
website: https://greencommuteinitiative.uk/
trustpilot:
  stars: 5
  reviews: 34
  url: https://uk.trustpilot.com/review/greencommuteinitiative.uk
summary: The only option for bikes > £1,000. Great reviews.
find_retailers_url: https://greencommuteinitiative.uk/shops/
logo: https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/7c2ed694b6948ceaf4b17449c335ca09/green-commute-initiative.png
---

The Green Commute Initiative differentiates itself by being the only (?) scheme provider to support bike purchases above £1,000. With other schemes, unless your company has a 'consumer credit licence'.

According to their [bike shop search tool](https://greencommuteinitiative.uk/shops/) they support a large selection of smaller and independent bike shops, certainly in London.
