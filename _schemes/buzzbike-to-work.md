---
layout: scheme
title: Buzzbike to Work
website: https://buzzbike.cc/buzzbike-to-work
trustpilot:
summary: Alternative salary-sacrifice rental (not ownership) scheme
terms_url: https://buzzbike.cc/termsofuse/
logo: https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/eae822a993d619de2eeeef515d4c0525/buzzbike.jpeg
---

This alternative to Cycle to Work allows renting a bike without any choice of bike, and without any option of ownership at the end of the hire agreement.

With the Buzzbike subscription (£29.99 per month), you get:

- a standard [Buzzbike](https://buzzbike.cc/whats-included/#ExploreYourRide) for your exclusive use
- lights and locks for the bike
- insurance
- bike servicing, including 'call a mechanic' in-app if you break down
- no commitment; you can cancel any time
- an app to track your stats and compare yourself to colleagues

The employer doesn't need to contribute any money towards the bike or scheme.

This option is geared towards new cyclists who don't want to choose a specific bike.

It's based in London, but available throughout the UK.
