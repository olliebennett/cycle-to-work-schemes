---
layout: scheme
title: CycleScheme
website: https://www.cyclescheme.co.uk/
trustpilot:
  stars: 3
  reviews: 53
  url: https://uk.trustpilot.com/review/www.cyclescheme.co.uk
summary: The "most popular" option. Worst reviews.
find_retailers_url: https://www.cyclescheme.co.uk/retailers
logo: https://gitlab.com/olliebennett/cycle-to-work-schemes/uploads/5c81de2391b4515abb80474fe03507db/cyclescheme.png
---

This is the most widespread scheme.

CycleScheme support 2,000+ retailers nationwide ([src](https://help.cyclescheme.co.uk/article/56-where-can-i-shop-with-cyclescheme)).

The scheme is "completely free of charge service to employers and their employees" ([src](https://help.cyclescheme.co.uk/article/74-who-is-cyclescheme))

There's a useful [Advanced Savings Calculator](https://www.cyclescheme.co.uk/calculator) and also some details about [ownership fees](https://help.cyclescheme.co.uk/article/42-what-is-an-ownership-fee).

### Other Notes

Also offers [TechScheme](https://www.techscheme.co.uk/) and offers a bunch of other "incentives and engagement solutions" via [Blackhawk Network](https://www.blackhawknetworkeurope.com/).

