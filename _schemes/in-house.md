---
layout: scheme
title: In House Schemes
summary: The 'Do It Yourself' option; flexible and cheap but more hassle.
---


It's possible for the company to manage the Cycle to Work scheme themselves.

The company would need to manage the salary sacrifice and hire agreements themselves, although pre-made versions of some documents are available to puchase online.

By managing the Cycle to Work scheme internally, a company can avoid any fees paid to the scheme administrator.

For employees, an 'in house' scheme enables buying from any shop; payment can be made in cash.

