---
layout: default
order: 0
title: Home
permalink: /
---

## What is Cycle to Work?

The *Cycle to Work* scheme encourages UK employees to get cycling more by offering tax incentives when buying a new bike and/or equipment.

The employer typically pays up front for the (discounted) bike, then the employee repays the cost through salary sacrifice over a period of (typically) 1 year.

To use Cycle to Work, the employer first [registers for a cycle to work scheme]({{ site.baseurl }}{% link employer-registration.md %}), then the employee [follows some simple steps]({{ site.baseurl }}{% link employee-registration.md %}) to purchase the bike from a participating shop using a voucher instead of cash payment.

This (independent) site gives unbiased comparisons to help employers decide between different Cycle to Work providers, or even [manage the scheme themselves]({{ site.baseurl }}{% link _schemes/in-house.md %}).

## Comparison of Cycle to Work Schemes

See [this page]({{ site.baseurl }}{% link availability.md %}) for which schemes are accepted in different stores.

<table class="table table-bordered">
  <thead>
    <tr>
      <th>Scheme</th>
      <th>Summary</th>
      <th style="width: 150px;">Rating</th>
    </tr>
  </thead>
  <tbody>
    {% assign schemes_list = site.schemes | sort: 'title' %}
    {% for scheme in schemes_list %}
      <tr>
        <td>
          <a href="{{ scheme.url | prepend: site.baseurl }}">
            {{ scheme.title }}
          </a>
          {% if scheme.logo %}
            <img src="{{ scheme.logo }}" height="50px" class="float-right" />
          {% endif %}
        </td>
        <td>{{ scheme.summary }}</td>
        <td>
          {% if scheme.trustpilot %}
            {% for i in (1..scheme.trustpilot["stars"]) %}⭐️ {% endfor %}
            <br>
            ({{ scheme.trustpilot["reviews"] }} <a href="{{ scheme.trustpilot["url"] }}">reviews</a>)
          {% else %}
            -
          {% endif %}
        </td>
      </tr>
    {% endfor %}
  </tbody>
</table>


<table class="table table-bordered">
  <thead>
  </thead>
  <tbody id="comparison-summary">

  </tbody>
  <tbody id="comparison-bike-disposal">
    <tr>
      <th colspan="11">
        Bike Disposal
        <small>This section covers what happens behave after the hire agreement completes.</small>
      </th>
    </tr>
    <tr>
      <th>Hire Agreement Terms</th>
      <td>

So, there's a really deceptive clause in the CycleScheme contract; TL:DR CycleScheme get the bike after the 1 year.

> 4.2 At the end of each Hire Agreement (however that occurs, including for the avoidance of doubt, where the relevant Employee leaves the employment of the Employer), then unless otherwise agreed in writing with the Provider, the ownership of the relevant Equipment will transfer from the Employer to the Provider in consideration of the provision of the Service at no cost to the Employer by Cyclescheme and with full title guarantee.

> 4.3 Following any transfer of ownership under clause 4.2 above, the Provider will dispose of the relevant Equipment as it sees fit. For the avoidance of doubt, any such disposal will be made by the Provider in its own capacity as owner of the Equipment and will not be made on behalf of the Employer (whether as its agent or otherwise) and, consequently, all proceeds from that disposal will belong exclusively to the Provider.


      </td>
      <td>
        <blockquote>Bike 2 Work will assist the Employer with the disposal of the relevant Bike Equipment (i.e. the Bike Equipment hired out to an Employee under that Hire Agreement) in accordance with the arrangements set out in clauses 5.2 to 5.4 below.</blockquote>




        5.2 The ownership of the asset will transfer from the employer to Bike 2 Work Scheme, at the end of the contract, in consideration for the provision of certain administration services by Bike 2 Work Scheme, including but not exclusively the collection of the bike, if required, at no cost.

        5.3 Following the transfer of Bike Equipment ownership under clause 5.2 above, Bike 2 Work will offer to sell the relevant Bike Equipment to the relevant Employee (or to such other person as the Employer may direct) at a price not exceeding the fair market value of the Bike Equipment at the time, as reasonably determined by Bike 2 Work. Any such offer will be made by Bike 2 Work in its own capacity as owner of the Bike Equipment and will not be made on behalf of the Employer and, consequently, if any such offer is subsequently accepted, then all proceeds from that sale will belong exclusively with the Employer having no claim thereon.

        5.4 If after the expiry of 90 days from the date on which a transfer of ownership occurs under clause 5.2 above the Employer has not issued any request to Bike 2 Work under clause 5.3 or the Employer has issued such a request, but the person to whom the Bike Equipment was offered by Bike 2 Work has not accepted the relevant offer, then Bike 2 Work will be free to dispose of the relevant Bike Equipment in such manner as Bike 2 Work reasonably and legally thinks fit.
      </td>
    </tr>
  </tbody>
</table>
