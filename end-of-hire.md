---
layout: page
order: 5
title: End of Hire
submenu: resources
---

- There are some common misunderstandings around the "end of hire period" and what needs to be done to deal with ownership of the bike.

- Since the bike is actually leased, the terms typically state that the bike transfers from the employer's ownership to the scheme provider after the hire/salary-sacrifice period ends (normally 12 months).

- After the hire period, you have three options;
  - Defer ownership for 3/4/5 _more_ years, (sometimes for a reasonable one-off 'ownership fee') with no further monthly payments, after which time the bike transfers to you.
  - Pay the HMRC Fair Market Value for the bike up-front.
  - Send or return the bike to the cyclescheme provider or shop with postage at your own expense.

- If the employee leaves the company before their hire period finishes, ([src](https://help.cyclescheme.co.uk/article/82-what-happens-if-an-employee-leaves-their-job-or-is-made-redundant)), they must pay the outstanding amount (without any tax breaks) from their final salary. An end of hire ownership fee may still apply.
