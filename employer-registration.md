---
layout: page
order: 3
title: Employer Registration
---

You'll need the following information;
<ul>
  <li>Company Name + Address</li>
  <li>Number of UK PAYE employees</li>
  <li>Category (eg. "Technology" or "Insurance")</li>
  <li>VAT Registration Number (if the company has one)</li>
  <li>Employer <abbr title="National Insurance Contributions">NIC</abbr> rate (your accountants will know!).</li>
  <li>% of NIC to pass to employees (if any)</li>
  <li>"Main contact" or "Administrator" details; name, job title, phone number and email address</li>
  <li>The same details for any other contact to be kept involved.</li>
  <li>Email address for invoices (if different to above)</li>
</ul>
