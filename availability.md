---
layout: page
order: 5
title: Shop / Scheme Availability Matrix
submenu: resources
---

This page outlines which schemes are available through which shops.

{% assign schemes_list = site.schemes | sort: 'title' %}
{% assign shops_list = site.shops | sort: 'title' %}

<table class="table table-bordered">
  <thead>
    <tr>
      <th>&nbsp;</th>
      {% for shop in shops_list %}
        <th class="text-center align-middle"><a href="{{ shop.url | prepend: site.baseurl }}">{{ shop.title | replace: " ", "<br>" }}</a></th>
      {% endfor %}
    </tr>
  </thead>
  <tbody>
    {% for scheme in schemes_list %}
      <tr>
        <th><a href="{{ scheme.url | prepend: site.baseurl }}">{{ scheme.title }}</a></th>
        {% for shop in shops_list %}
          <td class="text-center">
          {% assign scheme_availability = shop.schemes_available[scheme.slug] %}
          {% if scheme_availability["support"] == true %}
            <span class="badge badge-success">YES</span>
          {% elsif scheme_availability["support"] == false %}
            <span class="badge badge-danger">NO</span>
          {% elsif scheme_availability["support"] == 'other' %}
            {% if scheme_availability["details"] %}
              <span class="badge badge-warning" data-toggle="tooltip" title="{{ scheme_availability["details"] }}">INFO</span>
            {% else %}
              <span class="badge badge-warning">INFO</span>
            {% endif %}
          {% else %}
            <span class="badge badge-default">?</span>
          {% endif %}
          {% if scheme_availability["src"] %}
            <a href="{{ scheme_availability['src'] }}">&rarr;</a>
          {% endif %}
          </td>
        {% endfor %}
      </tr>
    {% endfor %}
  </tbody>
</table>

