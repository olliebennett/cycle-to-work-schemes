---
layout: shop
title: Cycles UK
website: https://www.cyclesuk.com/
schemes_available:
  bike2work:
    support: true
    src: https://www.cyclesuk.com/pages/cycle-to-work/
  buzzbike-to-work:
    support: false
  cycle2work:
    support: true
    src: https://www.cyclesuk.com/pages/cycle-to-work/
  cycles-uk:
    support: true
  cyclesolutions:
    support: true
    src: https://www.cyclesuk.com/pages/cycle-to-work/
  halfords:
    support: true
    src: https://www.cyclesuk.com/pages/cycle-to-work/
  in-house:
    support: true
---

About Cycles UK Shop

Cycles UK have 12 stores in the South East of England, but deliver (free) nationwide.
