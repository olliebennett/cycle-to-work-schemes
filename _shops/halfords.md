---
layout: shop
title: Halfords
website: https://www.halfords.com/
schemes_available:
  buzzbike-to-work:
    support: false
  cycle2work:
    support: true
  in-house:
    support: true
---

About Halfords Shop

Note: Halfords own Cycle Republic; from a Cycle to Work perspective they're interchangeable.
