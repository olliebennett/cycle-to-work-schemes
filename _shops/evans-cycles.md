---
layout: shop
title: Evans Cycles
website: https://www.evanscycles.com/
schemes_available:
  bike2work:
    support: true
    src: https://www.evanscycles.com/b2b/cycle-to-work-scheme
  buzzbike-to-work:
    support: false
  cyclescheme:
    support: true
    src: https://www.evanscycles.com/b2b/cycle-to-work-scheme
  cycle-plus:
    support: true
    src: https://www.evanscycles.com/b2b/cycle-to-work-scheme
  evans-cycles-ride-to-work:
    support: true
    src: https://www.evanscycles.com/b2b/cycle-to-work-scheme
  in-house:
    support: true
  green-commute-initiative:
    support: true
    src: https://greencommuteinitiative.uk/shops/
---

About Evans Cycles Shop
