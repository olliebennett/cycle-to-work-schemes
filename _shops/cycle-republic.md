---
layout: shop
title: Cycle Republic
website: https://www.cyclerepublic.com/
schemes_available:
  bike2work:
    support: true
    src: https://www.cyclerepublic.com/cycle-to-work-schemes
  buzzbike-to-work:
    support: false
  cyclescheme:
    support: true
    src: https://www.cyclerepublic.com/cycle-to-work-schemes
  cycle2work:
    support: true
    src: https://www.cyclerepublic.com/cycle-to-work-schemes
  in-house:
    support: true
---

Cycle Republic only accept Cycle to Work vouchers on items marked with the corresponding logo, although it seems almost all adult hybrid, mountain and road bikes are eligible (even those above £1000), and including sale items.

Depending on your Cycle to Work scheme, you may not be able to spend more than £1000 and sale items may not be allowed.

Note: Halfords own Cycle Republic; from a Cycle to Work perspective they're interchangeable.
