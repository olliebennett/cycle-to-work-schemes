---
layout: shop
title: Decathlon
website: https://www.decathlon.com/
schemes_available:
  cyclescheme:
    support: true
  bike2work:
    support: true
  buzzbike-to-work:
    support: false
  cycle-plus:
    support: other
    details: Some stores only
    src: https://www.decathlon.co.uk/cycle-to-work.html
  cycle2work:
    support: false
  in-house:
    support: true
---

About Decathlon Shop
