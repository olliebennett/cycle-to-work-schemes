---
layout: shop
title: Independent Shops
schemes_available:
  buzzbike-to-work:
    support: false
  cycle2work:
    support: 'count'
    store_count: 1
  in-house:
    support: true
---

There are thousands of independent bike shops around the UK; it would be impossible to list which of them support which schemes.

The best approach is to use the scheme's website to check for eligible shops.
